package io.spaceboost.interview;

import java.util.List;

public class AlgorithmItem {

    private int id;
    private String name;
    private String version;
    private List<Integer> keywordsId;


    public AlgorithmItem(int id, String name, String version, List<Integer> keywordsId) {
        this.id = id;
        this.name = name;
        this.version = version;
        this.keywordsId = keywordsId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public List<Integer> getKeywordsId() {
        return keywordsId;
    }

    public void setKeywordsId(List<Integer> keywordsId) {
        this.keywordsId = keywordsId;
    }

}
