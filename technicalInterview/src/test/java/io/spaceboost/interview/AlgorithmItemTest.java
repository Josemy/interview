package io.spaceboost.interview;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class AlgorithmItemTest {

    /**
     * Why does this test pass? They are the same items, but the test says they are not.
     *
     * Make it pass.
     */
    @Test
    public void test1() {
        AlgorithmItem item1 = new AlgorithmItem(1, "ROI", "v2-202221", Arrays.asList(1, 2, 3));
        AlgorithmItem item2 = new AlgorithmItem(1, "ROI", "v2-202221", Arrays.asList(1, 2, 3));
        assertEquals(item1, item2);
    }

    /**
     * Why does this test pass? keywordsId is the element that you modified adding the number 5, not item1,
     * but the test says that they are equals.
     *
     * Make it pass.
     */
    @Test
    public void test2() {
        List<Integer> keywordsId = new ArrayList<Integer>();
        keywordsId.add(2);
        keywordsId.add(3);
        keywordsId.add(4);
        AlgorithmItem item1 = new AlgorithmItem(1, "ROI", "v2-202221", keywordsId);

        assertEquals(item1.getKeywordsId(), keywordsId);

        keywordsId.add(5);
        assertNotEquals(item1.getKeywordsId(), keywordsId);
    }


}