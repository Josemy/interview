# Technical Interview

## Requirements

* JRE 8+
* [Postman 6+](https://www.getpostman.com/)

## What to do?

You will have a file ```spaceboost-collection-postman.json``` which you should import into Postman.

## How to import collection? 

What you need to do is open *Postman* and click: **Import** > **Choose file** > **Select ```spaceboost-collection-postman.json```** > **Open**. With that you should have now a *Collection* on your *Postman* called **SpaceboostAPI** with some request predefined.

![postman_spaceboost_collection](images/postman_spaceboost_collection.png)

Your challenge, if you choose to accept it, will be to make every request to work. Every request has some **test** that will pass when the requirement has been accomplished and the **blue** dot will turn to **green**.

![request_test_example](images/request_test_example.png)

If you want, you can also import the file ```spaceboost-environment-postman``` which will define some environment variable in Postman that will be useful for the requests.

```
apiUrl = http://localhost:8080/api
EMAIL = joe@what.com
PASSWORD = password
token = "" // To be setted on loggin request
```

Finally, you should write a small report, explaining everything that you assumed and your technical choises (Spring, Vertx, Play, H2, HashMap, ¿? ...). No need to be more that one page.
